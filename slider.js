var start = setInterval(function(){avanzaSlide()},3000);
 
//un Array con las clases para las imagenes
var arrayImagenes = new Array(".img1",".img2",".img3",".img4");
 
//variable que indica la primera foto
var contador = 0;
 
//El efecto chidori que ven como opacity
function mostrar(img){
  $(img).ready(function(){        
      $(arrayImagenes[contador]).fadeIn(1000);    
  });
}
 
function ocultar(img){
  $(img).ready(function(){        
      $(arrayImagenes[contador]).fadeOut(1000);   
  });
}

//Lean bien esto
function avanzaSlide(){
        //se oculta la imagen actual
   ocultar(arrayImagenes[contador]);
        //aumentamos el contador en una unidad
    contador = (contador + 1) % 4;
        //mostramos la nueva imagen
    mostrar(arrayImagenes[contador]);
}
